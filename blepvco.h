/* blepvco - minBLEP-based, hard-sync-capable LADSPA VCOs.
 *
 * Copyright (C) 2004-2005 Sean Bolton.
 *
 * Much of the LADSPA framework used here comes from VCO-plugins
 * 0.3.0, copyright (c) 2003-2004 Fons Adriaensen.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307, USA.
 */

#ifndef _BLEPVCO_H
#define _BLEPVCO_H

#include "minblep_tables.h"

#include <ladspa.h>
#include "ladspaplugin.h"


class Ladspa_minBLEP_VCO : public LadspaPlugin
{
public:
    enum { FILLEN = 256 };

    Ladspa_minBLEP_VCO (unsigned long fsam) : LadspaPlugin (fsam) {}
    virtual void place_step_dd(float *buffer, int index, float phase, float w, float scale);
    virtual void place_slope_dd(float *buffer, int index, float phase, float w, float slope_delta);
    virtual ~Ladspa_minBLEP_VCO (void) {}
};

class Ladspa_VCO_blepsaw : public Ladspa_minBLEP_VCO
{
public:

    enum { OUTP, SYNCOUT, FREQ, EXPM, LINM, SYNCIN, OCTN, TUNE, EXPG, LING, FILT, NPORT };

    Ladspa_VCO_blepsaw (unsigned long fsam) : Ladspa_minBLEP_VCO (fsam) {}
    virtual void setport (unsigned long port, LADSPA_Data *data);  
    virtual void active  (bool act);
    virtual void runproc (unsigned long len, bool add);
    virtual ~Ladspa_VCO_blepsaw (void) {}

private:

    float   *_port [NPORT];
    float   _p, _w, _z;
    float   _f [FILLEN + STEP_DD_PULSE_LENGTH];
    int     _j, _init;
};


class Ladspa_VCO_bleprect : public Ladspa_minBLEP_VCO
{
public:

    enum { OUTP, SYNCOUT, FREQ, EXPM, LINM, WAVM, SYNCIN, OCTN, TUNE, EXPG, LING, WAVE, WMDG, FILT, NPORT };

    Ladspa_VCO_bleprect (unsigned long fsam) : Ladspa_minBLEP_VCO (fsam) {}
    virtual void setport (unsigned long port, LADSPA_Data *data);  
    virtual void active  (bool act);
    virtual void runproc (unsigned long len, bool add);
    virtual ~Ladspa_VCO_bleprect (void) {}

private:

    float   *_port [NPORT];
    float   _p, _w, _b, _x, _z;
    float   _f [FILLEN + STEP_DD_PULSE_LENGTH];
    int     _j, _k, _init;
};


class Ladspa_VCO_bleptri : public Ladspa_minBLEP_VCO
{
public:

    enum { OUTP, SYNCOUT, FREQ, EXPM, LINM, WAVM, SYNCIN, OCTN, TUNE, EXPG, LING, WAVE, WMDG, FILT, NPORT };

    Ladspa_VCO_bleptri (unsigned long fsam) : Ladspa_minBLEP_VCO (fsam) {}
    virtual void setport (unsigned long port, LADSPA_Data *data);  
    virtual void active  (bool act);
    virtual void runproc (unsigned long len, bool add);
    virtual ~Ladspa_VCO_bleptri (void) {}

private:

    float   *_port [NPORT];
    float   _p, _w, _b, _z;
    float   _f [FILLEN + LONGEST_DD_PULSE_LENGTH];
    int     _j, _k, _init;
};


#endif /* _BLEPVCO_H */
