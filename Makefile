
CPPFLAGS += -I. -fPIC -D_REENTRANT -Wall -O3


blepvco.so:	blepvco.o blepvco_if.o exp2ap.o minblep_tables.o
	g++ -shared  blepvco.o blepvco_if.o exp2ap.o minblep_tables.o -o blepvco.so

blepvco.o:	ladspaplugin.h blepvco.h minblep_tables.h
blepvco_if.o:	ladspaplugin.h blepvco.h


install:
	/bin/cp  blepvco.so /usr/lib/ladspa


DIR := $(shell basename `pwd`)

archive:	clean
	cd ..; /bin/rm -f $(DIR).tar.bz2; tar cvf $(DIR).tar $(DIR); bzip2 $(DIR).tar


clean:
	/bin/rm -f *~ *.o *.so

